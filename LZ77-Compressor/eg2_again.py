#!/usr/bin/python2
# Python3 

from LZ77 import LZ77Compressor

# argv.py
import sys

direction = sys.argv[1]
ifile = sys.argv[2]
ofile = sys.argv[3]

print "Input file:  ", ifile 
print "Output file: ", ofile 

compressor = LZ77Compressor(window_size=20) # window_size is optional

  
if direction == "c":
	print("Compressing {0} and sending to {1}..." .format(ifile, ofile)) 
	compressor.compress(ifile, ofile)
else:
	print("Decompressing {0} and sending to {1}..." .format(ifile, ofile)) 
	compressor.decompress(ifile, ofile)
  

print("done");