#!/usr/bin/python3
# Python3 

from LZ77 import LZ77Compressor

# argv.py
import sys

direction = sys.argv[1]
ifile = sys.argv[2]
ofile = sys.argv[3]

print( "Input file:  ", ifile)
print( "Output file: ", ofile)

compressor = LZ77Compressor(window_size=20) # window_size is optional

  
if direction == "c":
	print( "Compressing {0} and sending to {1}..." .format(ifile, ofile) )
	compressor.compress(ifile, ofile)
else:
	print( "Decompressing {0} and sending to {1}..." .format(ifile, ofile) )
	compressor.decompress(ifile, ofile)

print("done");


import lzma
data = b"Insert Data Here"
with lzma.open("file.xz", "w") as f:
	f.write(data)

data_in = b"Insert Data Here"
data_out = lzma.compress(data_in)

with open("file.xz", "wb") as f:
	f.write(b"This data will not be compressed\n")
	with lzma.open(f, "w") as lzf:
		lzf.write(b"This *will* be compressed\n")
	f.write(b"Not compressed\n")
