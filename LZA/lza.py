#!/usr/bin/python2
# Python3 

# argv.py
import sys
import os
import lz4.frame

#direction = sys.argv[1]
#ifile = sys.argv[2]
#ofile = sys.argv[3]

#print "Input file:  ", ifile 
#print "Output file: ", ofile 

#if direction == "c":
#	print("Compressing {0} and sending to {1}..." .format(ifile, ofile)) 
#	ofile = lz4.frame.compress(ifile)
#else:
#	print("Decompressing {0} and sending to {1}..." .format(ifile, ofile)) 
#	ofile = lz4.frame.decompress(ifile)
  
#print("done");

lz4.frame.COMPRESSIONLEVEL_MIN

with open("ibl_e3_1.0.294.bin", "rb") as p:
        file_content = p.read()

ifile = file_content

ofile = lz4.frame.compress(file_content)

with open("ibl_e3_1.0.294.lz4", "w") as c:
		c.write(ofile)

c.close()
p.close()

with open("ibl_e3_1.0.294.lz4", "r") as c:
        file_content = c.read()

dfile = lz4.frame.decompress(file_content)
 
with open("ibl_e3_1.0.294.lz4d", "w") as d:
                d.write(dfile)

if ifile == dfile:
	print("IBL: original file and decompressed file match") 
else:
	print("IBL: shit happens")


with open("encharge-1.2.2118_release-20.19_b3df687.bin", "rb") as p:
        file_content = p.read()

ifile = file_content

ofile = lz4.frame.compress(file_content)

with open("encharge-1.2.2118_release-20.19_b3df687.lz4", "w") as c:
                c.write(ofile)

c.close()
p.close()

with open("encharge-1.2.2118_release-20.19_b3df687.lz4", "r") as c:
        file_content = c.read()

dfile = lz4.frame.decompress(file_content)

with open("encharge-1.2.2118_release-20.19_b3df687.lz4d", "w") as d:
                d.write(dfile)

if ifile == dfile:
        print("CHG: original file and decompressed file match")
else:
        print("CHG: shit happens")


with open("enpower-1.2.1305_release-20.19_3c9ebe1.bin", "rb") as p:
        file_content = p.read()

ifile = file_content

ofile = lz4.frame.compress(file_content)

with open("enpower-1.2.1305_release-20.19_3c9ebe1.lz4", "w") as c:
                c.write(ofile)

c.close()
p.close()

with open("enpower-1.2.1305_release-20.19_3c9ebe1.lz4", "r") as c:
        file_content = c.read()

dfile = lz4.frame.decompress(file_content)

with open("enpower-1.2.1305_release-20.19_3c9ebe1.lz4d", "w") as d:
                d.write(dfile)

if ifile == dfile:
        print("PWR: original file and decompressed file match")
else:
        print("PWR: shit happens")


lz4.frame.COMPRESSIONLEVEL_MINHC

with open("encharge-1.2.2118_release-20.19_b3df687.bin", "rb") as p:
        file_content = p.read()

ifile = file_content

ofile = lz4.frame.compress(file_content, 3)

with open("encharge-1.2.2118_release-20.19_b3df687.lz4_HC", "w") as c:
                c.write(ofile)

c.close()
p.close()

with open("encharge-1.2.2118_release-20.19_b3df687.lz4_HC", "r") as c:
        file_content = c.read()

dfile = lz4.frame.decompress(file_content)

with open("encharge-1.2.2118_release-20.19_b3df687.lz4d_HC", "w") as d:
                d.write(dfile)

if ifile == dfile:
        print("CHG HC: original file and decompressed file match")
else:
        print("CHG HC: shit happens")


lz4.frame.COMPRESSIONLEVEL_MAX

with open("encharge-1.2.2118_release-20.19_b3df687.bin", "rb") as p:
        file_content = p.read()

ifile = file_content

ofile = lz4.frame.compress(file_content,16)

with open("encharge-1.2.2118_release-20.19_b3df687.lz4_MAX", "w") as c:
                c.write(ofile)

c.close()
p.close()

with open("encharge-1.2.2118_release-20.19_b3df687.lz4_MAX", "r") as c:
        file_content = c.read()

dfile = lz4.frame.decompress(file_content)

with open("encharge-1.2.2118_release-20.19_b3df687.lz4d_MAX", "w") as d:
                d.write(dfile)

if ifile == dfile:
        print("CHG MAX: original file and decompressed file match")
else:
        print("CHG MAX: shit happens")

